const API_HOST_PRODUCTION = ''
const API_HOST_TEST = 'https://o9n8z5l1lj.execute-api.ap-southeast-1.amazonaws.com/dev'
const APPLICATION_MODE = process.env.NODE_ENV
let API_HOST = ''

if (APPLICATION_MODE === 'production') API_HOST = API_HOST_TEST
else API_HOST = API_HOST_TEST

const API_ROUTER = {
  ACCOUNT: {
    url: 'account',
  },
  AUTH: {
    url: 'auth',
  },
  SERVICE_EMAIL: {
    url: 'services/email_checker',
  },
  GET_ALL_COURSES: {
    url: 'courses',
  },
  GET_COURSE_DETAILS: {
    url: 'course/param',
  },
  GET_STUDENTS: {
    url: 'accounts?filter=type&qr=STUDENT',
  },
  GET_TEACHERS: {
    url: 'accounts?filter=type&qr=TEACHER',
  },
  GET_ACCOUNT_BY_ID: {
    url: 'account/param',
  },
  GET_ORDERS: {
    url: 'orders',
  },
  UPDATE_ORDER: {
    url: 'order/param',
  },
  GET_COURSE_DETAIL: {
    url: 'course/param',
  },
  GET_ORDER_BY_TEACHER: {
    url: 'order/teacher/param',
  },
  ORDER_SESSION: {
    url: '/order/param/sessions',
  },
  UPDATE_SESSION: {
    url: '/order/param1/sessions/param2',
  },
  UPDATE_SESSION_RESCHEDULE: {
    url: 'order/param1/sessions/param2',
  },
  SCHEDULE_TEACHER: {
    url: 'schedule/teacher',
  },
  SCHEDULE_TEACHER_BY_ID: {
    url: 'schedule/teacher/param',
  },
  GET_ALL_ACTIVITY: {
    url: 'admin/activity',
  },
  GET_COURSE_LEVEL_ID: {
    url: 'course/level/param',
  },
  ACCOUNT_ID: {
    url: 'account/param',
  },
}

export const getAPIRouter = (api) => {
  let result = ''
  switch (api) {
    case 'ACCOUNT': {
      const ROUTER = API_ROUTER.ACCOUNT
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'AUTH': {
      const ROUTER = API_ROUTER.AUTH
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'SERVICE_EMAIL': {
      const ROUTER = API_ROUTER.SERVICE_EMAIL
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_ALL_COURSES': {
      const ROUTER = API_ROUTER.GET_ALL_COURSES
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_COURSE_DETAILS': {
      const ROUTER = API_ROUTER.GET_COURSE_DETAILS
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_ACCOUNT_BY_ID': {
      const ROUTER = API_ROUTER.GET_ACCOUNT_BY_ID
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_STUDENTS': {
      const ROUTER = API_ROUTER.GET_STUDENTS
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_TEACHERS': {
      const ROUTER = API_ROUTER.GET_TEACHERS
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_ORDERS': {
      const ROUTER = API_ROUTER.GET_ORDERS
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_COURSE_DETAIL': {
      const ROUTER = API_ROUTER.GET_COURSE_DETAIL
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'UPDATE_ORDER': {
      const ROUTER = API_ROUTER.UPDATE_ORDER
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_ORDER_BY_TEACHER': {
      const ROUTER = API_ROUTER.GET_ORDER_BY_TEACHER
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'ORDER_SESSION': {
      const ROUTER = API_ROUTER.ORDER_SESSION
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'UPDATE_SESSION': {
      const ROUTER = API_ROUTER.UPDATE_SESSION
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'UPDATE_SESSION_RESCHEDULE': {
      const ROUTER = API_ROUTER.UPDATE_SESSION_RESCHEDULE
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'SCHEDULE_TEACHER': {
      const ROUTER = API_ROUTER.SCHEDULE_TEACHER
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'SCHEDULE_TEACHER_BY_ID': {
      const ROUTER = API_ROUTER.SCHEDULE_TEACHER_BY_ID
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_ALL_ACTIVITY': {
      const ROUTER = API_ROUTER.GET_ALL_ACTIVITY
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_COURSE_LEVEL_ID': {
      const ROUTER = API_ROUTER.GET_COURSE_LEVEL_ID
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'ACCOUNT_ID': {
      const ROUTER = API_ROUTER.ACCOUNT_ID
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    default: {
      result = ''
    }
  }
  return result
}
