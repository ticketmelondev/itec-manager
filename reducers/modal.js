const initState = {
  open: false,
}

const modal = (state = initState, action) => {
  switch (action.type) {
    case 'SET_OPEN_MODAL':
      return {
        open: action.open,
      }
    default:
      return state
  }
}

export default modal
