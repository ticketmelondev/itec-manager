const initState = {
  textSearch: '',
  originalOrders: [],
  allOrders: {
    pendingOrders: [],
    orders: [],
  },
  allOrderOriginal: {
    pendingOrders: [],
    orders: [],
  },
  orderDetail: {},
  levels: [],
  teachers: [],
  courseDetails: [],
}
const orders = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ORIGINAL_ORDERS':
      return {
        ...state,
        originalOrders: action.originalOrders,
      }
    case 'SET_ALL_ORDER_ORIGINAL':
      return {
        ...state,
        allOrderOriginal: action.allOrderOriginal,
      }
    case 'SET_ALL_ORDERS':
      return {
        ...state,
        allOrders: action.allOrders,
      }
    case 'SET_ORDER_DETAIL':
      return {
        ...state,
        orderDetail: action.orderDetail,
      }
    case 'SET_ORDER_DETAIL_LEVELS':
      return {
        ...state,
        levels: action.orderDetail,
      }
    case 'SET_ORDER_DETAIL_TEACHERS':
      return {
        ...state,
        teachers: action.orderDetail,
      }
    case 'SET_COURSE_DETAILS':
      return {
        ...state,
        courseDetails: action.courseDetails,
      }
    case 'SET_TEXT_SEARCH':
      return {
        ...state,
        textSearch: action.textSearch,
      }
    default:
      return state
  }
}

export default orders
