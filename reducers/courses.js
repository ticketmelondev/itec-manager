const initState = {
  allCourses: [],
  course: {},
  isNotFoundCourses: false,
}
const courses = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ALL_COURSES':
      return {
        ...state,
        allCourses: action.allCourses,
      }
    case 'SET_COURSE':
      return {
        ...state,
        course: action.course,
      }
    case 'SET_NOT_FOUND_COURS':
      return {
        ...state,
        isNotFoundCourses: action.isNotFoundCourses,
      }
    default:
      return state
  }
}

export default courses
