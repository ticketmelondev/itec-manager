const initState = {
  allTeachers: [],
  selectTeacher: [],
  calendars: {},
  selectCalendar: [],
  bookSlotStep: 1,
  bookSlotSuccess: {},
  bookSlotRemove: {},
  thisTeacherId: '',
  studentDetails: [],
  viewSession: {},
}
const teacher = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ALL_TEACHERS':
      return {
        ...state,
        allTeachers: action.allTeachers,
      }
    case 'SET_SESSION_RESCHEDULE':
      return {
        ...state,
        selectTeacher: action.selectTeacher,
      }
    case 'SET_SELECT_TEACHER_ID':
      return {
        ...state,
        thisTeacherId: action.thisTeacherId,
      }
    case 'SET_CALENDAR':
      return {
        ...state,
        calendars: action.calendar,
      }
    case 'SET_SELECT_CALENDAR':
      return {
        ...state,
        selectCalendar: action.selectCalendar,
      }
    case 'SET_BOOKSLOT_STEP':
      return {
        ...state,
        bookSlotStep: action.bookSlotStep,
      }
    case 'SET_BOOK_SUCCESS':
      return {
        ...state,
        bookSlotSuccess: action.bookSlotSuccess,
      }
    case 'SET_BOOK_REMOVE':
      return {
        ...state,
        bookSlotRemove: action.bookSlotRemove,
      }
    case 'SET_STUDENT_DETAILS':
      return {
        ...state,
        studentDetails: action.studentDetails,
      }
    case 'SET_VIEW_SESSION':
      return {
        ...state,
        viewSession: action.viewSession,
      }
    default:
      return state
  }
}

export default teacher
