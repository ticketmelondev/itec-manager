import React, { useEffect } from 'react'
import { Router } from '@router'

import Login from '@containers/Login'
import { useMember } from '@containers/auth'

function LoginPage() {
  const { isAuthenticated } = useMember()
  useEffect(() => {
    if (isAuthenticated === true) {
      location.href = '/'
    }
  }, [])

  return <Login />
}

export default LoginPage
