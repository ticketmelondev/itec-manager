import React, { Fragment } from 'react'
import { Helmet } from 'react-helmet'
import App from 'next/app'
import Router from 'next/router'
// import { Provider } from 'mobx-react'
import { CookiesProvider } from 'react-cookie'

import { ThemeProvider } from '@material-ui/core/styles'
import theme from '@lib/theme'
import { StylesProvider } from '@material-ui/core/styles'

import { withAuth } from '@containers/auth'
// import { initStore } from '@lib/store'
import * as font from '@lib/styles/font'
import GlobalStyle from '@lib/styles/GlobalStyle'

class MyApp extends App {
  componentDidMount() {
    const WebFont = require('webfontloader')
    WebFont.load(font.config)

    Router.events.on('routeChangeStart', (url) => {
      if (window.__NEXT_DATA__.props.isSSR === undefined) {
        window.__NEXT_DATA__.props.isSSR = false
      }
    })

    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles)
    }
  }

  render() {
    const { Component, router } = this.props

    const children = (
      <Fragment>
        <GlobalStyle />
        {/* <Helmet titleTemplate={`%s - nextweb.js`} /> */}
        <StylesProvider injectFirst>
          <ThemeProvider theme={theme}>
            <Component {...this.props.pageProps} router={router} />
          </ThemeProvider>
        </StylesProvider>
      </Fragment>
    )

    if (process.browser) {
      return <CookiesProvider>{children}</CookiesProvider>
    }

    return children
  }
}

export default withAuth(MyApp)
// export default MyApp
