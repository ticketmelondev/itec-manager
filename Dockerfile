FROM node:alpine
#FROM keymetrics/pm2
# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
#COPY pm2.json .
#RUN npm install --production
RUN rm -rf node_modules/
RUN npm cache clean --force 
RUN npm install
RUN npm install pm2 -g

# Bundle app source
COPY . /usr/src/app

#RUN pm2 link xmmsa9a98t9261g t7s66ztv4rdml12
RUN chmod -R u+x node_modules/next/dist/bin
RUN npm run build

EXPOSE 3500
CMD [ "npm", "run", "production" ]
#CMD [ "pm2-runtime","production", "pm2.json" ]
#CMD [ "pm2-docker", "pm2.json", "process.yml"]
