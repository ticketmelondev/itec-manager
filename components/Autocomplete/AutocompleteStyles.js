import { makeStyles } from '@material-ui/core/styles'

const AutocompleteStyles = makeStyles((theme) => ({
  root: {},
  paper: {
    margin: '8px 0',
    boxShadow: 'inset 0px 0px 4px rgba(0, 0, 0, 0.12)',
  },
  listbox: {
    padding: '8px 8px',
  },
  option: {
    paddingLeft: '8px',
    paddingRight: '8px',
    minHeight: 48,
  },
  icon: {
    color: theme.palette.primary.main,
  },
  endAdornmentIcon: {
    color: 'rgba(0, 0, 0, 0.5)',
  },
}))

export default AutocompleteStyles
