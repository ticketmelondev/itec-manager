import { makeStyles } from '@material-ui/core/styles'

const IconRoundedStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  titleTypography: {
    fontWeight: 600,
    fontSize: '0.875rem',
    lineHeight: '140%',
    color: theme.palette.primary.main,
  },
  avatarRounded: {
    color: '#fff',
    backgroundColor: theme.palette.primary.main,
    width: 28,
    height: 28,
    marginRight: 12,
    textAlign: 'center',
    fontSize: '1.125rem',
  },
}))

export default IconRoundedStyles
