import { makeStyles } from '@material-ui/core/styles'

const TextUnderlineStyles = makeStyles((theme) => ({
  root: {
    display: 'inline-block',
    position: 'relative',
    color: '#333333',
    fontSize: 42,
    lineHeight: '51px',
    letterSpacing: '-0.02em',
    '&:after': {
      content: `''`,
      position: 'absolute',
      width: '100%',
      transform: 'scaleX(1)',
      height: '5px',
      bottom: -5,
      left: 0,
      backgroundColor: theme.palette.primary.main,
      transformOrigin: 'bottom right',
      transition: 'transform 0.25s ease-out',
    },
  },
}))

export default TextUnderlineStyles
