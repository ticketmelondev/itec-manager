import { makeStyles } from '@material-ui/core/styles'

const CircularLoading = makeStyles((theme) => ({
  loading: {
    textAlign: 'center',
    color: theme.palette.primary.main,
  },
}))

export default CircularLoading
