import { makeStyles } from '@material-ui/core/styles'

const InputComponentStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 360,
    // maxHeight: 64,
    minHeight: 64,
    flexDirection: 'inherit',
    [theme.breakpoints.down('xs')]: {
      minWidth: '100%',
    },
  },
  fullWidth: {
    minWidth: '100%',
  },
  root: {
    border: 0,
    overflow: 'hidden',
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    borderLeft: `3px solid #FFFFFF`,
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    color: 'rgba(0, 0, 0, 0.7)',
    fontSize: '16px',
    lineHeight: '24px',
    '&:hover': {
      backgroundColor: '#fff',
      // borderLeft: `3px solid ${theme.palette.primary.main}`,
    },
    '&$focused': {
      backgroundColor: '#fff',
      borderLeft: `3px solid ${theme.palette.primary.main}`,
    },
    '&$disabled': {
      backgroundColor: '#fff',
      // border: '1px solid #E1E1E1'
    },
  },
  focused: {
    backgroundColor: '#fff',
    borderLeft: `3px solid ${theme.palette.primary.main}`,
  },
  label: {
    color: 'rgba(0, 0, 0, 0.5)',
    fontSize: '1rem',
    transform: 'translate(15px, 24px) scale(1)',
    '&$labelFocused': {
      color: '#808080',
      fontSize: '0.75rem',
    },
    '&$shrink': {
      transform: 'translate(15px, 10px) scale(0.75)',
      fontSize: '0.75rem',
    },
    '&$disabled': {
      color: '#CCCCCC',
    },
  },
  labelFocused: {
    color: '#808080',
    fontSize: '0.75rem',
  },
  shrink: {
    transform: 'translate(15px, 10px) scale(0.75)',
    fontSize: '0.75rem',
  },
  inputAdornment: {
    color: theme.palette.primary.main,
    height: 'auto',
  },
  outlined: {
    border: '1px solid #CCCCCC',
    '&$disabled': {
      border: '1px solid #E1E1E1',
    },
  },
  disabled: {},
  adornedEnd: {
    paddingRight: 0,
  },
}))

export default InputComponentStyles
