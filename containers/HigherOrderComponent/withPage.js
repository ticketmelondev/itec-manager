import { flowRight as compose } from 'lodash'

import withLayout from './withLayout'
import withRestrictedPage from './withRestrictedPage'

export default function withPage(options = {}) {
  return function (Component) {
    const hocs = [withRestrictedPage(options.restricted), withLayout(options.layout)]

    return compose(...hocs)(Component)
  }
}
