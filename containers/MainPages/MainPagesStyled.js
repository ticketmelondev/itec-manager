import { makeStyles } from '@material-ui/core/styles'

const MainPagesStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#e5e5e5',
    height: '100vh',
  },
  main: {
    flexGrow: 0,
    maxWidth: 'calc(100% - 272px)',
    flexBasis: 'calc(100% - 272px)',
    marginLeft: '272px',
    backgroundColor: '#e5e5e5',
    [theme.breakpoints.down('sm')]: {
      maxWidth: '100%',
      flexBasis: '100%',
      marginLeft: 0,
    },
  },
}))

export default MainPagesStyles
