import Autocomplete from '@components/Autocomplete'
import Input from '@components/Input'
import InputSmall from '@components/InputSmall'

export function CustomInputLevel({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'LEVEL_ID')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}

export function CustomInputTeacher({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'TEACHER_ID')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}

export function CustomInputStatus({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'STATUS')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}

export function CustomInputPaymentDetail({ onChange, ...rest }) {
  const handleChange = (event) => {
    onChange(event.target.value, 'PAYMENT_DETAIL')
  }
  return <Input onChange={handleChange} {...rest} />
}

export function CustomInputSession({ onChange, ...rest }) {
  const handleChange = (event) => {
    onChange(event.target.value, 'SESSION_QUANTITY')
  }
  return <InputSmall onChange={handleChange} {...rest} />
}
