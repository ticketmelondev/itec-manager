import React, { Fragment, useState } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import classNames from 'classnames'
import moment from 'moment'
import Paper from '@material-ui/core/Paper'
import TableContainer from '@material-ui/core/TableContainer'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'
import { Pagination, PaginationItem } from '@material-ui/lab'
import withWidth, { isWidthDown } from '@material-ui/core/withWidth'

import * as Actions from '@actions/orderActions'
import * as AlertActions from '@actions/alertActions'

import Icon from '@components/Icon'
import IconRounded from '@components/IconRounded'
import Modal from '@components/Modal'
import CircularLoading from '@components/CircularLoading'
import { showOrderStatus } from '@util/orderFunc'
import { displayShortID, displayStudentShortID } from '@util/centerFunc'

import Booking from '../Booking'
import Details from '../Details'
import SessionDetail from '../Session'

import datatableStyles from './DatatableStyled'

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index])
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map((el) => el[0])
}

function EnhancedTableHead({ headCells, order, orderBy, onRequestSort, width }) {
  const classes = datatableStyles()
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }
  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            classes={{ head: classes.headTableCell }}
            key={headCell.id}
            align={headCell.alignCell ? headCell.alignCell : 'left'}
            sortDirection={orderBy === headCell.id ? order : false}
            // style={{ minWidth: isWidthDown('md', width) ? `${headCell.width}px` : 'auto' }}
            style={{ minWidth: `${headCell.width}px`, maxWidth: `${headCell.width}px` }}
          >
            {headCell.sort ? (
              <TableSortLabel
                classes={{
                  root: classes.rootTableSortLabel,
                  active: classes.activeTableSortLabel,
                  icon: classes.iconTableSortLabel,
                }}
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                IconComponent={ArrowDropDown}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            ) : (
              headCell.label
            )}
          </TableCell>
        ))}
        <TableCell classes={{ head: classes.headTableCell }} />
        <TableCell classes={{ head: classes.headTableCell }} />
        <TableCell classes={{ head: classes.headTableCell }} />
      </TableRow>
    </TableHead>
  )
}

function EnhancedTableBody({
  actions,
  data,
  order,
  orderBy,
  page,
  rowsPerPage,
  expanded,
  onClickExpand,
  orderDetail,
  levels,
  teachers,
  isLoadingDetail,
  isAlert,
  alertActions,
}) {
  const classes = datatableStyles()
  const [open, setOpen] = useState(false)
  const [openBook, setOpenBook] = useState(false)
  const handleClick = async (orderId, courseId) => {
    let detail = data.filter((order) => {
      return order.ORDER_ID === orderId
    })
    await actions.setOrderDetail(detail[0])
    await actions.getCourseDetail(courseId)
  }
  const handleExpanded = (id) => {
    onClickExpand(id)
  }
  return (
    <Fragment>
      <TableBody>
        {stableSort(data, getComparator(order, orderBy))
          .slice((page - 1) * rowsPerPage, (page - 1) * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            const iconExpandClasses = classNames({
              [classes.icon]: true,
              [classes.iconExpand]: true,
              [classes.iconDisabled]: row.SESSIONS.length === 0,
            })
            const iconBookingClasses = classNames({
              [classes.icon]: true,
              [classes.iconDisabled]: row.STATUS !== 'PAID',
            })
            const statusClasses = classNames({
              [classes.status]: true,
              [classes.statusSuccess]: row.STATUS === 'PAID',
              [classes.statusWaiting]: row.STATUS === 'WAITING' || row.STATUS === 'ORDER_CREATE',
              [classes.statusCancel]: row.STATUS === 'CANCEL',
            })
            return (
              <Fragment>
                <TableRow key={index}>
                  <TableCell
                    component="th"
                    scope="row"
                    classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}
                  >
                    {displayShortID(row.ORDER_ID)}
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    {displayStudentShortID(row.STUDENT_ID)}
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    {row.STUDENT}
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    {row.CONTACT_NO}
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    {row.COURSE_NAME}
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    {row.TEACHER}
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    <div className={statusClasses}>{showOrderStatus(row.STATUS)}</div>
                  </TableCell>
                  <TableCell
                    classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}
                    style={{ width: '20px', padding: '4px 0' }}
                  >
                    <IconButton
                      disabled={row.STATUS == 'PAID' ? false : true}
                      onClick={() => {
                        setOpenBook((prev) => !prev)
                        handleClick(row.ORDER_ID, row.COURSE_ID)
                      }}
                    >
                      <Icon className={iconBookingClasses}>calendarPlus</Icon>
                    </IconButton>
                  </TableCell>
                  <TableCell
                    classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}
                    style={{ width: '20px', padding: '4px 0' }}
                  >
                    <IconButton
                      onClick={() => {
                        setOpen((prev) => !prev)
                        handleClick(row.ORDER_ID, row.COURSE_ID)
                      }}
                    >
                      <Icon className={classes.icon}>editBoxOutline</Icon>
                    </IconButton>
                  </TableCell>
                  <TableCell
                    classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}
                    style={{ width: '20px', padding: '4px 24px 4px 0' }}
                  >
                    <IconButton
                      onClick={() => handleExpanded(row.ORDER_ID)}
                      style={{ padding: '6px' }}
                      disabled={row.SESSIONS.length > 0 ? false : true}
                    >
                      {expanded[row.ORDER_ID] ? (
                        <Icon className={iconExpandClasses}>expandLess</Icon>
                      ) : (
                        <Icon className={iconExpandClasses}>expandMore</Icon>
                      )}
                    </IconButton>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    colSpan={10}
                    style={{
                      padding: expanded[row.ORDER_ID] ? '8px 0' : 0,
                      borderBottom: expanded[row.ORDER_ID] ? '1px solid #e0e0e0' : 'unset',
                    }}
                  >
                    <Collapse
                      in={expanded ? expanded[row.ORDER_ID] : false}
                      timeout="auto"
                      unmountOnExit
                      className={classes.collapse}
                    >
                      <SessionDetail orderDetail={row} />
                    </Collapse>
                  </TableCell>
                </TableRow>
              </Fragment>
            )
          })}
      </TableBody>
      <OrderDetails
        open={open}
        detail={orderDetail}
        levels={levels}
        teachers={teachers}
        isLoading={isLoadingDetail}
        actions={actions}
        onClose={() => setOpen((prev) => !prev)}
      />
      <BookCourse
        open={openBook}
        detail={orderDetail}
        actions={actions}
        isAlert={isAlert}
        alertActions={alertActions}
        onClose={() => setOpenBook((prev) => !prev)}
      />
    </Fragment>
  )
}

function BookCourse({ open, detail, onClose, actions, isAlert, alertActions }) {
  return (
    <Modal
      open={open}
      onClose={onClose}
      headerChildren={<IconRounded title="Booking" icon={<Icon>bookmark</Icon>} />}
    >
      <Booking
        detail={detail}
        isAlert={isAlert}
        alertActions={alertActions}
        actions={actions}
        onClose={onClose}
      />
    </Modal>
  )
}
function OrderDetails({ open, onClose, detail, isLoading, actions, levels, teachers }) {
  return (
    <Modal
      open={open}
      onClose={onClose}
      headerChildren={<IconRounded title="Edit Order" icon={<Icon>edit</Icon>} />}
    >
      <Details
        detail={detail}
        levels={levels}
        teachers={teachers}
        isLoad={isLoading}
        actions={actions}
        onClose={onClose}
      />
    </Modal>
  )
}

function PaginationItemCustom({ item, pageAll }) {
  const classes = datatableStyles()
  const rootItemClasse = classNames({
    [classes.rootItem]: true,
    [classes.rootLastItem]: pageAll === item.page,
  })
  if (item.type === 'previous' || item.type === 'next') {
    return (
      <PaginationItem
        classes={{ root: classes.rootItemIcon, selected: classes.selectedItem }}
        {...item}
      />
    )
  } else {
    return (
      <PaginationItem
        classes={{
          root: rootItemClasse,
          page: classes.pageItem,
          selected: classes.selectedItem,
          ellipsis: classes.ellipsisItem,
          focusVisible: classes.focusVisible,
        }}
        {...item}
      />
    )
  }
}

function LoadingData() {
  const classes = datatableStyles()
  return (
    <TableBody>
      <TableRow>
        <TableCell colSpan={8} className={classes.loadingTableCell}>
          <CircularLoading />
        </TableCell>
      </TableRow>
    </TableBody>
  )
}

function EmptyData() {
  const classes = datatableStyles()
  return (
    <TableBody>
      <TableRow>
        <TableCell colSpan={8} className={classes.emptyTableCell}>
          No data to display
        </TableCell>
      </TableRow>
    </TableBody>
  )
}

function Datatable({
  keyField,
  columns,
  data,
  sizePerPage,
  width,
  page,
  onChangePage,
  expanded,
  onClickExpand,
  isLoading,
  isLoadingDetail,
  actions,
  orderDetail,
  levels,
  teachers,
  isAlert,
  alertActions,
}) {
  const classes = datatableStyles()
  const [order, setOrder] = useState('asc')
  const [orderBy, setOrderBy] = useState('ORDER_ID')
  const [rowsPerPage, setRowsPerPage] = useState(sizePerPage)
  const dataAll = data ? data.length : 0
  const pageAll = Math.ceil(dataAll / rowsPerPage)
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleChangePage = (event, newPage) => {
    onChangePage(newPage)
  }
  return (
    <Fragment>
      <TableContainer component={Paper} className={classes.paper}>
        <Table aria-label="collapsible table" id={keyField}>
          <EnhancedTableHead
            headCells={columns}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            width={width}
          />
          {isLoading ? (
            <LoadingData />
          ) : dataAll > 0 ? (
            <EnhancedTableBody
              data={data}
              order={order}
              orderBy={orderBy}
              page={page}
              rowsPerPage={rowsPerPage}
              expanded={expanded}
              onClickExpand={onClickExpand}
              actions={actions}
              orderDetail={orderDetail}
              levels={levels}
              teachers={teachers}
              isLoadingDetail={isLoadingDetail}
              isAlert={isAlert}
              alertActions={alertActions}
            />
          ) : (
            <EmptyData />
          )}
        </Table>
      </TableContainer>

      {dataAll > rowsPerPage ? (
        <Pagination
          count={pageAll}
          page={page}
          onChange={handleChangePage}
          classes={{ root: classes.rootPagination, ul: classes.ulPagination }}
          renderItem={(item) => <PaginationItemCustom item={item} pageAll={pageAll} />}
        />
      ) : null}
    </Fragment>
  )
}
Datatable.defaultProps = {
  sizePerPage: 10,
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoading.allOrdersLoading,
    isLoadingDetail: state.isLoading.orderDetailLoading,
    orderDetail: state.orders.orderDetail,
    levels: state.orders.levels,
    teachers: state.orders.teachers,
    isAlert: state.isAlert,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
    alertActions: bindActionCreators(AlertActions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps), withWidth())

export default enhancer(Datatable)
