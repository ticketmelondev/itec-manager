import { makeStyles } from '@material-ui/core/styles'

const datatableStyle = makeStyles((theme) => ({
  paper: {
    marginTop: '8px',
    boxShadow: 'unset',
    borderRadius: 'unset',
  },
  icon: {
    fontSize: '1rem',
    color: '#CCCCCC',
    cursor: 'pointer',
    overflow: 'unset',
  },
  iconExpand: { fontSize: '1.5rem' },
  iconDisabled: { color: '#E6E6E6' },
  rootTableCell: {
    fontWeight: 'normal',
    lineHeight: '1.25rem',
    borderBottom: '1px solid rgba(0, 0, 0, 0.06)',
  },
  headTableCell: {
    padding: '16px 8px',
    fontSize: '0.75rem',
    fontWeight: 600,
    lineHeight: '1.125rem',
    color: 'rgba(0, 0, 0, 0.56)',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  bodyTableCell: {
    padding: '4px 8px',
    color: 'rgba(0, 0, 0, 0.6)',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  emptyTableCell: {
    textAlign: 'center',
    color: '#999999',
  },
  loadingTableCell: {
    height: '180px',
  },
  rootTableSortLabel: {
    '&$activeTableSortLabel': {
      color: 'rgba(0, 0, 0, 0.56)',
    },
  },
  activeTableSortLabel: {},
  iconTableSortLabel: { opacity: 1 },
  status: {
    width: '85px',
    fontSize: '0.8125rem',
    fontWeight: 600,
    lineHeight: 1.5,
    textAlign: 'center',
    backgroundColor: '#CCCCCC',
    borderRadius: '5px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  statusSuccess: {
    color: '#2D7720',
    backgroundColor: '#D8F4D2',
  },
  statusWaiting: {
    color: '#83610D',
    backgroundColor: '#FFEBBA',
  },
  statusCancel: {
    color: '#B41B1B',
    backgroundColor: '#FFD8D8',
  },
  rootPagination: {
    padding: '16px',
  },
  ulPagination: {
    justifyContent: 'center',
    alignSelf: 'center',
    '& > li': {
      backgroundColor: '#FFFFFF',
      padding: '8px 0',
      '&:first-child': {
        borderTopLeftRadius: '5px',
        borderBottomLeftRadius: '5px',
        border: '1px solid #e0e0e0',
      },
      '&:last-child': {
        borderTopRightRadius: '5px',
        borderBottomRightRadius: '5px',
        border: '1px solid #e0e0e0',
      },
    },
  },
  rootItem: {
    minWidth: '35px',
    height: 'auto',
    margin: 0,
    padding: 0,
    color: '#BDBDBD',
    fontWeight: 600,
    lineHeight: 1.25,
    borderRight: '1px solid #E0E0E0',
    borderRadius: 'unset',
    '&:hover': {
      backgroundColor: 'transparent',
    },
    '&$selectedItem': {
      backgroundColor: 'transparent',
      color: '#ED1B24',
      fontWeight: '600',
      '&:hover': {
        backgroundColor: 'transparent',
      },
      '&$focusVisible': {
        backgroundColor: 'transparent',
      },
    },
  },
  rootItemIcon: {
    color: '#ED1B24',
    fontWeight: 600,
    lineHeight: 1.25,
    height: '17px',
    '&$selectedItem': {
      backgroundColor: 'transparent',
      color: '#ED1B24',
      fontWeight: '600',
    },
  },
  rootLastItem: {
    borderRight: 'none',
  },
  pageItem: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  selectedItem: {},
  focusVisible: {},
  ellipsisItem: { height: '20px' },
}))

export default datatableStyle
