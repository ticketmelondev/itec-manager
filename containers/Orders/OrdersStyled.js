import { makeStyles } from '@material-ui/core/styles'

const OrdersStyles = makeStyles((theme) => ({
  typographyH4: {
    color: 'rgba(0, 0, 0, 0.7)',
    fontSize: '2rem',
    fontWeight: 'normal',
    lineHeight: 2.4375,
    flexGrow: 1,
  },
  typographySubtitle1: {
    color: '#7A7A7A',
    fontWeight: 500,
    lineHeight: 1.5,
    flexGrow: 1,
  },
  buttonExport: {
    minWidth: '180px',
    height: '48px',
    marginLeft: '12px',
    [theme.breakpoints.between('sm', 'md')]: {
      width: '-webkit-fill-available',
      marginLeft: '12px',
    },
    [theme.breakpoints.down('xs')]: {
      width: '100%',
      marginLeft: 0,
      marginTop: '8px',
    },
  },
  rootPendingOrders: {
    marginTop: '16px',
  },
  rootOrders: {
    marginTop: '32px',
    marginBottom: '48px',
  },
  iconExport: {
    marginLeft: '10px',
    fontSize: '1.125rem',
  },
}))

export default OrdersStyles
