import React, { Fragment, useState } from 'react'
import classNames from 'classnames'
import moment from 'moment'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Collapse from '@material-ui/core/Collapse'

import Button from '@components/Button'
import CircularLoading from '@components/CircularLoading'
import { Alert } from '@components/Snackbar'

import { getHours, getMinutes, displayTime } from '@util/formatDate'
import { bookingType, sessionNumber } from '@util/bookingFunc'

import {
  CustomInputBookingType,
  CustomInputDate,
  CustomInputTimeHour,
  CustomInputTimeMinute,
  CustomInputSession,
} from './Field'
import BookingStyle from './BookingStyled'

function Booking({ onClose, detail, isLoading, actions, isAlert, alertActions }) {
  const classes = BookingStyle()
  const [state, setState] = useState({
    type: '',
    date: '',
    time_hour: '',
    time_min: '',
  })
  const onChange = async (value, name) => {
    let data = {}
    if (name === 'type') {
      data = {
        ...state,
        [name]: value,
        session: { label: '1', value: 1 },
        date: '',
        time_hour: '',
        time_min: '',
      }
    } else if (name === 'date') {
      data = {
        ...state,
        [name]: value,
        time_hour: '',
        time_min: '',
      }
    } else if (name === 'time_hour') {
      data = {
        ...state,
        [name]: value,
      }
    } else if (name === 'time_min') {
      data = {
        ...state,
        [name]: value,
      }
    } else if (name === 'session') {
      data = {
        ...state,
        [name]: value,
      }
    }
    setState(data)
  }
  const handleClose = () => {
    setState({
      type: '',
      date: '',
      time_hour: '',
      time_min: '',
    })
    onClose()
  }
  return (
    <div className={classes.root}>
      {isLoading ? (
        <CircularLoading />
      ) : (
        <Fragment>
          <div>
            <Grid container alignItems="center">
              <Grid items xs={4} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Course
                </Typography>
              </Grid>
              <Grid items xs={8} className={classes.infoSection}>
                <span style={{ fontWeight: 600 }}>{detail.COURSE_NAME}</span>
              </Grid>
              <Grid items xs={4} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Class Duration
                </Typography>
              </Grid>
              <Grid items xs={8} className={classes.infoSection}>
                {displayTime(detail.SESSION_TIME)}
              </Grid>
              <Grid items xs={4} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Session
                </Typography>
              </Grid>
              <Grid items xs={8} className={classes.infoSection}>
                {detail.SESSIONS.length}/{detail.SESSION_QUANTITY}
              </Grid>

              <Grid items xs={12} className={classes.infoSection}>
                <CustomInputBookingType
                  name="type"
                  label="Booking Type"
                  options={bookingType}
                  value={state.type}
                  onChange={onChange}
                  fullWidth
                />
              </Grid>
              {state.type && ['daily', 'weekly'].includes(state.type.value) && (
                <Grid items xs={12} className={classes.infoSection}>
                  <CustomInputSession
                    label="Number of session"
                    name="session"
                    onChange={onChange}
                    value={state.session}
                    options={sessionNumber(detail.SESSION_QUANTITY, detail.SESSIONS.length)}
                    fullWidth
                  />
                </Grid>
              )}
              <Grid items xs={12} className={classes.infoSection}>
                <Typography variant="subtitle2" component="div" className={classes.subtitle2}>
                  Please select Date and Time
                </Typography>
              </Grid>
              <Grid items xs={12} className={classes.infoSection}>
                <CustomInputDate
                  name="date"
                  label="Date"
                  value={state.date}
                  onChange={onChange}
                  disabled={!state.type}
                  minDate={new Date(moment())}
                  outlined
                  fullWidth
                />
              </Grid>
              <Grid items xs={12} className={classes.infoSection}>
                <div
                  className={classNames({
                    [classes.boxInputTime]: true,
                    [classes.boxInputTimeDisabled]: !state.date,
                  })}
                >
                  <div className={classes.labelTime}>Class Time</div>
                  <Grid container>
                    <Grid items xs={6}>
                      <CustomInputTimeHour
                        name="time_hour"
                        label="HH"
                        options={getHours()}
                        value={state.time_hour}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                        disabled={!state.date}
                      />
                    </Grid>
                    <Grid items xs={6}>
                      <CustomInputTimeMinute
                        name="time_min"
                        label="MM"
                        options={getMinutes()}
                        value={state.time_min}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                        disabled={!state.date}
                      />
                    </Grid>
                  </Grid>
                </div>
              </Grid>
            </Grid>
          </div>
          <div className={classes.alertRoot}>
            <Collapse in={isAlert.open}>
              <Alert
                classes={{ filledError: classes.alert, icon: classes.alertIcon }}
                severity={'error'}
                onClose={alertActions.handleClose}
              >
                {`ไม่สามารถเริ่มจอง ${moment(isAlert.text[0]).format('D MMM YYYY HH:mm')} ได้`}
              </Alert>
            </Collapse>
          </div>
          <div className={classes.buttonSection}>
            <Button
              type="submit"
              color="primary"
              size="large"
              fullWidth
              disabled={!state.time_hour && !state.time_min}
              onClick={() => {
                actions.createSession(state)
                handleClose()
              }}
            >
              Submit Request
            </Button>
          </div>
        </Fragment>
      )}
    </div>
  )
}
export default Booking
