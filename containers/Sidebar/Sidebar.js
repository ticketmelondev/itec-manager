import React from 'react'
import Typography from '@material-ui/core/Typography'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'

import Icon from '@components/Icon'
import { useMember } from '@containers/auth'
import { getStatic } from '../../lib/static'

import SidebarStyles from './SidebarStyled'
import { signOut } from '@services/cookie'

function Sidebar({ menu, onChange }) {
  const classes = SidebarStyles()
  const handleChange = (menu) => {
    onChange(menu)
  }

  const { profile } = useMember()

  return (
    <div className={classes.root}>
      <div className={classes.logo}>
        <img src={`${getStatic('static/images/Eduworld.png')}`} alt="img" style={{ width: 120 }} />
      </div>
      <div>
        <MenuList>
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
              gutters: classes.guttersMenuItem,
              selected: classes.selectedMenuItem,
            }}
            onClick={(e) => handleChange('orders')}
            selected={menu === 'orders'}
          >
            <ListItemIcon classes={{ root: classes.rootListItemIcon }}>
              <Icon className={classes.iconMenu}>server</Icon>
            </ListItemIcon>
            <Typography variant="inherit">Order</Typography>
          </MenuItem>
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
              gutters: classes.guttersMenuItem,
              selected: classes.selectedMenuItem,
            }}
            onClick={(e) => handleChange('teachers')}
            selected={menu === 'teachers'}
          >
            <ListItemIcon classes={{ root: classes.rootListItemIcon }}>
              <Icon className={classes.iconMenu}>briefcase</Icon>
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              Teachers
            </Typography>
          </MenuItem>
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
              gutters: classes.guttersMenuItem,
              selected: classes.selectedMenuItem,
            }}
            onClick={(e) => handleChange('students')}
            selected={menu === 'students'}
          >
            <ListItemIcon classes={{ root: classes.rootListItemIcon }}>
              <Icon className={classes.iconMenu}>student</Icon>
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              Students
            </Typography>
          </MenuItem>
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
              gutters: classes.guttersMenuItem,
              selected: classes.selectedMenuItem,
            }}
            onClick={(e) => handleChange('activity')}
            selected={menu === 'activity'}
          >
            <ListItemIcon classes={{ root: classes.rootListItemIcon }}>
              <Icon className={classes.iconMenu}>activity</Icon>
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              Activity Log
            </Typography>
          </MenuItem>
        </MenuList>
      </div>
      <div className={classes.user}>
        <hr className={classes.hr} />
        <div className={classes.userInfo}>
          {`${profile?.FISRT_NAME} ${profile?.LAST_NAME}`}
          <br />
          <span className={classes.userEmail}>{profile?.EMAIL}</span>
        </div>
        <div
          className={classes.userEmail}
          style={{ padding: '16px 24px', cursor: 'pointer' }}
          onClick={() => signOut()}
        >
          Log Out
        </div>
      </div>
    </div>
  )
}

export default Sidebar
