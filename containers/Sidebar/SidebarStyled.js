import { makeStyles } from '@material-ui/core/styles'

const SidebarStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 0,
    maxWidth: '272px',
    flexBasis: '272px',
    height: '100vh',
    width: '272px',
    backgroundColor: '#FFFFFF',
    position: 'fixed',
    zIndex: 1,
    top: 0,
    left: 0,
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  logo: {
    height: '40px',
    padding: '16px 24px',
  },
  user: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
  },
  userInfo: {
    padding: '16px 24px',
    fontWeight: '600',
    fontSize: '1.125rem',
    lineHeight: '1.75rem',
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  userEmail: {
    fontSize: '0.875rem',
    fontWeight: 'normal',
    lineHeight: '1.25rem',
    letterSpacing: '0.02em',
    color: 'rgba(122, 122, 122, 0.7)',
  },
  hr: {
    margin: '27px 16px',
    border: 'none',
    borderBottom: '1px solid #DEDEDE',
  },
  rootMenuItem: {
    paddingTop: '16px',
    paddingBottom: '16px',
    fontSize: '0.875rem',
    fontWeight: '500',
    lineHeight: '1.25rem',
    color: '#9B9B9B',
    '&:hover': {
      backgroundColor: 'rgba(237, 27, 35, 0.04)',
      color: '#ED1B24',
      fontWeight: 'bold',
      // borderRight: '5px solid #ED1B24',
    },
    '&$selectedMenuItem': {
      color: '#ED1B24',
      fontWeight: 'bold',
      backgroundColor: 'rgba(237, 27, 35, 0.04)',
      '&:hover': {
        backgroundColor: 'rgba(237, 27, 35, 0.04)',
      },
    },
  },
  guttersMenuItem: {
    paddingLeft: '24px',
  },
  selectedMenuItem: {},
  rootListItemIcon: {
    minWidth: 'inherit',
    color: 'currentColor',
  },
  iconMenu: {
    marginRight: '16px',
    fontSize: '1.375rem',
  },
}))

export default SidebarStyles
