import { makeStyles } from '@material-ui/core/styles'

const TeachersStyles = makeStyles((theme) => ({
  typographyH4: {
    flexGrow: 1,
    fontSize: '2rem',
    fontWeight: 'normal',
    lineHeight: 2.4375,
    color: 'rgba(0, 0, 0, 0.7)',
  },
  typographySubtitle1: {
    fontWeight: 600,
    lineHeight: '1.5rem',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  typographyBody1: {
    fontWeight: 600,
    color: 'rgba(0, 0, 0, 0.8)',
  },
  typographyBody2: {
    fontSize: '0.8125rem',
    fontWeight: 'normal',
    lineHeight: '1.25rem',
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  typographyH5: {
    fontWeight: 'normal',
    lineHeight: '1.8125rem',
    color: '#000000',
  },
  typographyCaption: {
    fontSize: '0.8125rem',
    fontWeight: 'normal',
    lineHeight: '1.25rem',
    letterSpacing: ' 0.02em',
    color: 'rgba(0, 0, 0, 0.5)',
  },
  paper: {
    backgroundColor: '#FFFFFF',
    boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.12)',
    borderRadius: '5px',
    marginTop: '8px',
  },
  backSection: {
    fontWeight: 600,
    fontSize: '0.875rem',
    lineHeight: '1.5rem',
    color: '#ED1B24',
    [theme.breakpoints.down('md')]: {
      marginTop: '16px',
    },
  },
  section: {
    marginTop: '24px',
    marginBottom: '48px',
  },
  root: {
    boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.12)',
    borderRadius: '5px',
    cursor: 'pointer',
  },
  header: {
    padding: '16px 24px',
  },
  action: {
    marginTop: 0,
  },
  actionContent: {
    minWidth: '24px',
    height: '24px',
    padding: '0 8px',
    fontSize: '1rem',
    fontWeight: 600,
    lineHeight: '24px',
    borderRadius: '5px',
    backgroundColor: '#ED1B24',
    color: '#FFF',
    textAlign: 'center',
    boxSizing: 'border-box',
  },
  iconButton: {
    marginLeft: 10,
  },
  iconBack: {
    marginRight: '8px',
    fontSize: '0.75rem',
  },
  sectionButton1: {
    textAlign: 'right',
  },
  buttonReschedule: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('xs')]: {
      marginTop: '8px',
      justifyContent: 'center',
    },
  },
  buttonBookSlot: {
    padding: '16px',
    lineHeight: '1.0625rem',
    minWidth: '180px',
    [theme.breakpoints.down('xs')]: {
      marginTop: '16px',
      minWidth: '100%',
    },
  },
  rescheduleSection: {
    padding: '24px',
    backgroundColor: '#F8F7F7',
    borderBottomLeftRadius: '5px',
    borderBottomRightRadius: '5px',
  },
  rescheduleInfoSection: {
    marginTop: '16px',
    backgroundColor: '#FFF',
  },
  rescheduleInfo: {
    padding: '16px 24px',
    borderBottom: '1px solid rgba(0, 0, 0, 0.06)',
  },
  newSchedule: {
    fontWeight: 600,
    color: 'rgba(0, 0, 0, 0.8)',
  },
  iconForward: {
    fontSize: '9px',
    margin: '0 24px',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  buttonApprove: {
    minWidth: '104px',
    lineHeight: '1.25rem',
    padding: '5px 0',
    '&.outlined': {
      border: '1px solid #CCC',
      color: 'rgba(0, 0, 0, 0.7)',
      '&:hover': {
        border: '1px solid #CCC',
        color: 'rgba(0, 0, 0, 0.7)',
        backgroundColor: 'transparent',
      },
    },
  },
  buttonDeny: {
    minWidth: '104px',
    lineHeight: '1.25rem',
    padding: '5px 0',
    '&.outlined': {
      border: 'unset',
      '&:hover': {
        backgroundColor: 'transparent',
      },
    },
  },
  sectionTitle: {
    fontSize: '1.125rem',
    fontWeight: 600,
    lineHeight: '1.75rem',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  scheduleBox: {
    height: '345px',
    padding: '24px',
    background: '#FFFFFF',
    borderRadius: '5px',
    overflow: 'hidden',
  },
}))

export default TeachersStyles
