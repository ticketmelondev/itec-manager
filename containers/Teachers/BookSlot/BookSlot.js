import React, { useState, useEffect, Fragment } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import classNames from 'classnames'
import moment from 'moment'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import * as Actions from '@actions/teacherActions'

import Modal from '@components/Modal'
import Dialog from '@components/Dialog'
import Button from '@components/Button'
import Icon from '@components/Icon'
import IconRounded from '@components/IconRounded'
import CircularLoading from '@components/CircularLoading'

import { getHours, getMinutes } from '@util/formatDate'
import { bookingType } from '@util/bookingFunc'

import {
  CustomInputTitle,
  CustomInputBookingType,
  CustomInputStartDate,
  CustomInputStartTimeHour,
  CustomInputStartTimeMinute,
  CustomInputEndDate,
  CustomInputEndTimeHour,
  CustomInputEndTimeMinute,
} from './Field'

import BookSlotStyle from './BookSlotStyled'

function BookSlotForm({ onStep, detail, actions }) {
  const classes = BookSlotStyle()
  const [state, setState] = useState({
    TITLE: '',
    TYPE: { value: 'once', label: 'Once' },
    START_DATE: '',
    START_TIME_HOUR: '',
    START_TIME_MIN: '',
    END_DATE: '',
    END_TIME_HOUR: '',
    END_TIME_MIN: '',
  })
  const [minEndDate, setMinEndDate] = useState(null)
  useEffect(() => {
    setMinEndDate(new Date(moment()))
  }, [])
  const onChange = async (event, name) => {
    let data = {}
    if (name === 'TITLE') {
      data = {
        ...state,
        [name]: event,
      }
    } else if (name === 'TYPE') {
      data = {
        ...state,
        [name]: event,
        START_DATE: '',
        START_TIME_HOUR: '',
        START_TIME_MIN: '',
        END_DATE: '',
        END_TIME_HOUR: '',
        END_TIME_MIN: '',
      }
    } else if (name === 'START_DATE') {
      if (state.TYPE === 'once') {
        data = {
          ...state,
          [name]: event,
          START_TIME_HOUR: '',
          START_TIME_MIN: '',
          END_DATE: '',
          END_TIME_HOUR: '',
          END_TIME_MIN: '',
        }
      } else {
        data = {
          ...state,
          [name]: event,
          END_DATE: '',
        }
      }

      setMinEndDate(new Date(moment(event)))
    } else if (name === 'START_TIME_HOUR') {
      data = {
        ...state,
        [name]: event,
      }
    } else if (name === 'START_TIME_MIN') {
      data = {
        ...state,
        [name]: event,
      }
    } else if (name === 'END_DATE') {
      data = {
        ...state,
        [name]: event,
      }
    } else if (name === 'END_TIME_HOUR') {
      data = {
        ...state,
        [name]: event,
      }
    } else if (name === 'END_TIME_MIN') {
      data = {
        ...state,
        [name]: event,
      }
    }
    setState(data)
  }
  const onBooking = async () => {
    await actions.createSchedule(state)
    onStep(2)
  }
  return (
    <Grid container alignItems="center">
      <Grid items xs={3} className={classes.infoSection}>
        <Typography variant="body2" className={classes.body2}>
          Teacher
        </Typography>
      </Grid>
      <Grid items xs={9} className={classes.infoSection}>
        <span style={{ fontWeight: 600 }}>{detail ? detail.fullName : null}</span>
      </Grid>
      <Grid items xs={12} className={classes.infoSection}>
        <CustomInputTitle
          name="TITLE"
          label="Title"
          value={state.TITLE}
          className={classes.margin}
          fullWidth
          onChange={onChange}
          outlined
          // error={errors.status}
          // errorText={errors.status && errors.message}
        />
      </Grid>
      <Grid items xs={12} className={classes.infoSection}>
        <CustomInputBookingType
          name="TYPE"
          label="Booking Type"
          options={bookingType}
          value={state.TYPE}
          disabled={!state.TITLE}
          onChange={onChange}
          fullWidth
        />
      </Grid>
      {state.TYPE && state.TYPE.value === 'once' ? (
        <Fragment>
          <Grid items xs={12} className={classes.infoSection}>
            <Typography variant="body2" className={classes.title}>
              Start
            </Typography>
            <Grid container alignItems="center">
              <Grid items xs={6}>
                <div style={{ paddingRight: '4px' }}>
                  <CustomInputStartDate
                    name="START_DATE"
                    label="Date"
                    value={state.START_DATE}
                    onChange={onChange}
                    minDate={new Date()}
                    disabled={!state.TITLE}
                    fullWidth
                    outlined
                  />
                </div>
              </Grid>
              <Grid items xs={6} className={classes.infoSection}>
                <div
                  className={classNames({
                    [classes.boxInputTime]: true,
                    [classes.boxInputTimeDisabled]: !state.START_DATE,
                  })}
                  style={{ marginLeft: '4px' }}
                >
                  <div className={classes.labelTime}>Time</div>
                  <Grid container>
                    <Grid items xs={6}>
                      <CustomInputStartTimeHour
                        name="START_TIME_HOUR"
                        label="HH"
                        options={getHours()}
                        value={state.START_TIME_HOUR}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                        disabled={!state.TYPE}
                      />
                    </Grid>
                    <Grid items xs={6}>
                      <CustomInputStartTimeMinute
                        name="START_TIME_MIN"
                        label="MM"
                        options={getMinutes()}
                        value={state.START_TIME_MIN}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                        disabled={!state.TYPE}
                      />
                    </Grid>
                  </Grid>
                </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid items xs={12} className={classes.infoSection}>
            <Typography variant="body2" className={classes.title}>
              End
            </Typography>
            <Grid container alignItems="center">
              <Grid items xs={6} className={classes.infoSection}>
                <div style={{ paddingRight: '4px' }}>
                  <CustomInputEndDate
                    name="END_DATE"
                    label="Date"
                    value={state.END_DATE}
                    disabled={!state.START_DATE}
                    onChange={onChange}
                    minDate={minEndDate}
                    fullWidth
                    outlined
                  />
                </div>
              </Grid>
              <Grid items xs={6} className={classes.infoSection}>
                <div
                  className={classNames({
                    [classes.boxInputTime]: true,
                    [classes.boxInputTimeDisabled]: !state.END_DATE,
                  })}
                  style={{ marginLeft: '4px' }}
                >
                  <div className={classes.labelTime}>Time</div>
                  <Grid container>
                    <Grid items xs={6}>
                      <CustomInputEndTimeHour
                        name="END_TIME_HOUR"
                        label="HH"
                        options={getHours()}
                        value={state.END_TIME_HOUR}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                      />
                    </Grid>
                    <Grid items xs={6}>
                      <CustomInputEndTimeMinute
                        name="END_TIME_MIN"
                        label="MM"
                        options={getMinutes()}
                        value={state.END_TIME_MIN}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                      />
                    </Grid>
                  </Grid>
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      ) : null}
      {state.TYPE && ['daily', 'weekly'].includes(state.TYPE.value) && (
        <Fragment>
          <Grid items xs={12} className={classes.infoSection}>
            <Typography variant="body2" className={classes.title}>
              Time
            </Typography>
            <Grid container>
              <Grid items xs={6} className={classes.infoSection}>
                <div className={classes.boxInputTime} style={{ marginRight: '4px' }}>
                  <div className={classes.labelTime}>From</div>
                  <Grid container>
                    <Grid items xs={6}>
                      <CustomInputStartTimeHour
                        name="START_TIME_HOUR"
                        label="HH"
                        options={getHours()}
                        value={state.START_TIME_HOUR}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                        disabled={!state.TYPE}
                      />
                    </Grid>
                    <Grid items xs={6}>
                      <CustomInputStartTimeMinute
                        name="START_TIME_MIN"
                        label="MM"
                        options={getMinutes()}
                        value={state.START_TIME_MIN}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                        disabled={!state.TYPE}
                      />
                    </Grid>
                  </Grid>
                </div>
              </Grid>
              <Grid items xs={6} className={classes.infoSection}>
                <div className={classes.boxInputTime} style={{ marginLeft: '4px' }}>
                  <div className={classes.labelTime}>To</div>
                  <Grid container>
                    <Grid items xs={6}>
                      <CustomInputEndTimeHour
                        name="END_TIME_HOUR"
                        label="HH"
                        options={getHours()}
                        value={state.END_TIME_HOUR}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                      />
                    </Grid>
                    <Grid items xs={6}>
                      <CustomInputEndTimeMinute
                        name="END_TIME_MIN"
                        label="MM"
                        options={getMinutes()}
                        value={state.END_TIME_MIN}
                        onChange={onChange}
                        fullWidth
                        outlined={false}
                      />
                    </Grid>
                  </Grid>
                </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid items xs={12} className={classes.infoSection}>
            <Typography variant="body2" className={classes.title}>
              Date
            </Typography>
            <Grid container>
              <Grid items xs={6} className={classes.infoSection}>
                <div style={{ paddingRight: '4px' }}>
                  <CustomInputStartDate
                    name="START_DATE"
                    label="Date"
                    value={state.START_DATE}
                    onChange={onChange}
                    minDate={new Date()}
                    outlined
                    fullWidth
                  />
                </div>
              </Grid>
              <Grid items xs={6} className={classes.infoSection}>
                <div style={{ paddingLeft: '4px' }}>
                  <CustomInputEndDate
                    name="END_DATE"
                    label="Date"
                    value={state.END_DATE}
                    minDate={minEndDate}
                    onChange={onChange}
                    outlined
                    fullWidth
                  />
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Fragment>
      )}

      <Grid items xs={12} className={classes.infoSection}>
        <Button color="primary" size="large" fullWidth onClick={onBooking}>
          Book
        </Button>
      </Grid>
    </Grid>
  )
}

function BookSlotSuccess({ onClose, detail, dataSuccess }) {
  const classes = BookSlotStyle()
  return (
    <div className={classes.rootSuccess}>
      <div className={classes.section1}>
        <div className={classes.sectionIcon}>
          <div className={classes.rounedIcon}>
            <Icon style={{ color: '#FFF', fontSize: '28px' }}>check</Icon>
          </div>
        </div>
        <Typography variant="subtitle1" className={classes.subtitle1}>
          Time Slot has been booked
        </Typography>
        <Typography variant="body2" className={classes.notedText}>
          An email will be sent to notify the
          <br />
          updated schedule
        </Typography>
      </div>
      <div className={classes.section2}>
        <Grid container>
          <Grid items xs={3} className={classes.infoSection2}>
            Title
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography
              variant="body2"
              className={classNames({
                [classes.info]: true,
                [classes.infoBold]: true,
              })}
            >
              {dataSuccess.TITLE}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Teacher
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {detail ? `${detail.FISRT_NAME} ${detail.LAST_NAME}` : null}
            </Typography>
          </Grid>
          {dataSuccess.TYPE && dataSuccess.TYPE === 'once' ? (
            <Fragment>
              <Grid items xs={3} className={classes.infoSection2}>
                Start
              </Grid>
              <Grid items xs={9} className={classes.infoSection2}>
                <Typography variant="body2" className={classes.info}>
                  {`${moment(dataSuccess.START_DATE).format('ddd, D MMMM YYYY')} • ${
                    dataSuccess.START_TIME
                  }`}
                </Typography>
              </Grid>
              <Grid items xs={3} className={classes.infoSection2}>
                End
              </Grid>
              <Grid items xs={9} className={classes.infoSection2}>
                <Typography variant="body2" className={classes.info}>
                  {`${moment(dataSuccess.END_DATE).format('ddd, D MMMM YYYY')} • ${
                    dataSuccess.END_TIME
                  }`}
                </Typography>
              </Grid>
            </Fragment>
          ) : null}
          {dataSuccess.TYPE && ['daily', 'weekly'].includes(dataSuccess.TYPE) ? (
            <Fragment>
              <Grid items xs={3} className={classes.infoSection2}>
                Time
              </Grid>
              <Grid items xs={9} className={classes.infoSection2}>
                <Typography variant="body2" className={classes.info}>
                  {`${dataSuccess.START_TIME} - ${dataSuccess.END_TIME}`}
                </Typography>
              </Grid>
              <Grid items xs={3} className={classes.infoSection2}>
                Dates
              </Grid>
              <Grid items xs={9} className={classes.infoSection2}>
                <Typography variant="body2" className={classes.info}>
                  {`${moment(dataSuccess.START_DATE).format('D MMMM YYYY')} - ${moment(
                    dataSuccess.END_DATE,
                  ).format('D MMMM YYYY')}`}
                </Typography>
              </Grid>
            </Fragment>
          ) : null}
          <Grid items xs={12}>
            <Button fullWidth onClick={onClose} className={classes.buttonDone}>
              Done
            </Button>
          </Grid>
        </Grid>
      </div>
    </div>
  )
}

function BookSlot({ open, onClose, detail, actions, dataSuccess, isLoading, bookSlotStep }) {
  const classes = BookSlotStyle()
  const handelClose = () => {
    onClose()
    actions.setBookSlotStep(1)
    setSuccess((perv) => !perv)
  }
  const handleStep = (step) => {
    actions.setBookSlotStep(step)
    onClose()
    setSuccess((prev) => !prev)
  }
  const [success, setSuccess] = useState(false)
  return (
    <Fragment>
      {bookSlotStep === 1 && (
        <Modal
          open={open}
          onClose={handelClose}
          headerChildren={<IconRounded title="Book Slot" icon={<Icon>plus</Icon>} />}
        >
          <div className={classes.root}>
            <BookSlotForm onStep={handleStep} detail={detail} actions={actions} />
          </div>
        </Modal>
      )}
      {bookSlotStep === 2 && (
        <Dialog open={success}>
          {isLoading ? (
            <CircularLoading />
          ) : (
            <BookSlotSuccess
              detail={detail}
              dataSuccess={dataSuccess}
              onClose={() => {
                setSuccess((prev) => !prev)
                actions.setBookSlotStep(1)
              }}
            />
          )}
        </Dialog>
      )}
    </Fragment>
  )
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoading.bookSlotLoading,
    bookSlotStep: state.teacher.bookSlotStep,
    dataSuccess: state.teacher.bookSlotSuccess,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(BookSlot)
