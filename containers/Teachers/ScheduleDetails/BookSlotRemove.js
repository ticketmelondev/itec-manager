import React from 'react'
import moment from 'moment'
import classNames from 'classnames'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Button from '@components/Button'

import ScheduleDetailsStyles from './ScheduleDetailsStyles'

function BookSlotRemove({ onClose, actions, data, selectTeacher }) {
  const classes = ScheduleDetailsStyles()
  const onClickRemove = () => {
    actions.deleteSchedule()
    onClose()
  }
  return (
    <div className={classes.root}>
      <div className={classes.rootRemove}>
        <Grid container>
          <Grid items xs={3} className={classes.infoSection2}>
            Title
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography
              variant="body2"
              className={classNames({
                [classes.info]: true,
                [classes.infoBold]: true,
              })}
            >
              {data.TITLE}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Teacher
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {selectTeacher ? `${selectTeacher.FISRT_NAME} ${selectTeacher.LAST_NAME}` : null}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Start
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {`${moment(data.START_TIME).format('ddd, D MMMM YYYY • HH:mm')}`}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            End
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {`${moment(data.END_TIME).format('ddd, D MMMM YYYY • HH:mm')}`}
            </Typography>
          </Grid>
          <Grid items xs={12}>
            <Button fullWidth outlined onClick={onClickRemove} className={classes.buttonRemove}>
              Remove Booking
            </Button>
          </Grid>
        </Grid>
      </div>
    </div>
  )
}

export default BookSlotRemove
