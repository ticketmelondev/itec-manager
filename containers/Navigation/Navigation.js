import React, { useRef, useState } from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as modalActions from '@actions/modalActions'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Popper from '@material-ui/core/Popper'
import Paper from '@material-ui/core/Paper'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Slide from '@material-ui/core/Slide'
import Fade from '@material-ui/core/Fade'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import Typography from '@material-ui/core/Typography'
import { Button as MuiButton } from '@material-ui/core'
import NoSsr from '@material-ui/core/NoSsr'

// import Button from '@components/Button'
import Link from '@components/Link'
import Icon from '@components/Icon'
import { useMember } from '@containers/auth'

import NavigationStyles from './NavigationStyled'

import { getStatic } from '@lib/static'

import { signOut } from '@services/cookie'

const Button = dynamic(() => import('@components/Button'), {
  ssr: false,
  loading: () => null,
})
const IconButton = dynamic(() => import('@material-ui/core/IconButton'), {
  ssr: false,
  loading: () => null,
})

function MenuAccount({ profile }) {
  const [anchorEl, setAnchorEl] = useState(null)

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget)
  }

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popper' : undefined

  const classes = NavigationStyles()

  return (
    <div className={classes.buttonRoot}>
      <IconButton onClick={handleClick} aria-describedby={id}>
        <AccountCircleIcon />
      </IconButton>
      <Popper
        className={classes.popperAccount}
        id={id}
        open={open}
        anchorEl={anchorEl}
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Fade {...TransitionProps} timeout={500}>
            <Paper id="menu-list-growUser" className={classes.paperAccount}>
              <div className={classes.mobileRegisterRoot}>
                <div className={classes.account}>
                  <Typography className="title" variant="h5">
                    {profile?.FISRT_NAME || ''}
                  </Typography>
                  <Typography className="email">{profile?.EMAIL || ''}</Typography>
                </div>
                <div>
                  <MuiButton className={classes.muiButton}>
                    <Icon className={classes.iconButtonMenu}>server</Icon> Orders
                  </MuiButton>
                </div>
                <div>
                  <MuiButton className={classes.muiButton}>
                    <Icon className={classes.iconButtonMenu}>briefcase</Icon> Teachers
                  </MuiButton>
                </div>
                <div>
                  <MuiButton className={classes.muiButton}>
                    <Icon className={classes.iconButtonMenu}>student</Icon> Students
                  </MuiButton>
                </div>
                <div>
                  <MuiButton className={classes.muiButton}>
                    <Icon className={classes.iconButtonMenu}>activity</Icon> Activity Log
                  </MuiButton>
                </div>
                <div>
                  <MuiButton className={classes.muiButton} onClick={() => signOut()}>
                    <Icon className={classes.iconButtonMenu}>logout</Icon>
                    Log Out
                  </MuiButton>
                </div>
              </div>
            </Paper>
          </Fade>
        )}
      </Popper>
    </div>
  )
}

function NavigationMobile({ modal, modalActions, isAuthenticated, profile, onChange }) {
  const [menu, setMenu] = useState(false)
  const anchorRef = useRef(null)
  const handleChange = (menu) => {
    onChange(menu)
    setMenu(false)
  }

  const handleToggle = () => {
    event.preventDefault()
    setMenu((prevMenu) => !prevMenu)
    // pageConfigAction.setReponsiveToolbarOpen(menu)
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }
    setMenu(false)
    // pageConfigAction.setReponsiveToolbarOpen(true)
  }
  // const opacity = menu
  const classes = NavigationStyles({ opacity: menu })

  return (
    <div className={classes.iconButtonRoot}>
      <IconButton
        onClick={handleToggle}
        aria-owns={menu ? 'menu-list-user' : undefined}
        aria-haspopup="true"
      >
        <Icon>menu</Icon>
      </IconButton>
      <Popper
        className={classes.popperFullWidth}
        open={menu}
        // anchorEl={anchorRef.current}
        // placement="bottom"
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Fade {...TransitionProps} timeout={500}>
            <Paper id="menu-list-growUser" className={classes.paper}>
              <ClickAwayListener onClickAway={handleClose}>
                {isAuthenticated ? (
                  <div className={classes.mobileRegisterRoot}>
                    <div className={classes.account}>
                      <Typography className="title" variant="h5">
                        {profile?.FISRT_NAME || ''}
                      </Typography>
                      <Typography className="email">{profile?.EMAIL || ''}</Typography>
                    </div>
                    <div>
                      <MuiButton
                        className={classes.muiButton}
                        onClick={(e) => handleChange('orders')}
                      >
                        <Icon className={classes.iconButtonMenu}>server</Icon> Orders
                      </MuiButton>
                    </div>
                    <div>
                      <MuiButton
                        className={classes.muiButton}
                        onClick={(e) => handleChange('teachers')}
                      >
                        <Icon className={classes.iconButtonMenu}>briefcase</Icon> Teachers
                      </MuiButton>
                    </div>
                    <div>
                      <MuiButton
                        className={classes.muiButton}
                        onClick={(e) => handleChange('students')}
                      >
                        <Icon className={classes.iconButtonMenu}>student</Icon> Students
                      </MuiButton>
                    </div>
                    <div>
                      <MuiButton
                        className={classes.muiButton}
                        onClick={(e) => handleChange('activity')}
                      >
                        <Icon className={classes.iconButtonMenu}>activity</Icon> Activity Log
                      </MuiButton>
                    </div>
                    <div>
                      <MuiButton className={classes.muiButton} onClick={() => signOut()}>
                        <Icon className={classes.iconButtonMenu}>logout</Icon>
                        Log Out
                      </MuiButton>
                    </div>
                  </div>
                ) : (
                  <div className={classes.mobileRegisterRoot}>
                    <div className={classes.mobileLoginRoot}>
                      <span className="text">Already have an account?</span>
                      <span className="text-login">Log In</span>
                    </div>
                  </div>
                )}
              </ClickAwayListener>
            </Paper>
          </Fade>
        )}
      </Popper>
    </div>
  )
}

function Navigation({ modal, modalActions, onChange }) {
  const { open } = modal
  const classes = NavigationStyles()
  const { isAuthenticated, profile } = useMember()
  const handleChange = (menu) => {
    onChange(menu)
  }

  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar} position="fixed" color="inherit">
        <Toolbar className={classes.toolBar}>
          <div className={classes.title}>
            <Link route="home" passHref>
              <img
                src={`${getStatic('static/images/ECC_Logo.png')}`}
                alt="img"
                style={{ width: 92, cursor: 'pointer' }}
              />
            </Link>
          </div>
          <NoSsr>
            <div className={classes.buttonRoot}>
              {isAuthenticated ? (
                <MenuAccount profile={profile} />
              ) : (
                <Button
                  color="primary"
                  size="medium"
                  onClick={() => modalActions.setOpenModal(!open)}
                >
                  Login
                </Button>
              )}
            </div>
          </NoSsr>

          <NavigationMobile
            modal={modal}
            modalActions={modalActions}
            isAuthenticated={isAuthenticated}
            profile={profile}
            onChange={handleChange}
          />
        </Toolbar>
      </AppBar>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    modalActions: bindActionCreators(modalActions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(Navigation)
