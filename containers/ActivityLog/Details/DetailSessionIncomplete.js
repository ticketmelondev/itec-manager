import React from 'react'
import classNames from 'classnames'
import moment from 'moment'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'

import Icon from '@components/Icon'

import { showStatusSession } from '@util/orderFunc'

import DetailStyles from './DetailStyles'

function DetailSessionIncomplete({ row }) {
  const classes = DetailStyles()
  return (
    <TableBody>
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '145px' }}
        >
          <span className={classes.info}>Start Time</span>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '245px' }}
        >
          {row.OLD.START_TIME ? moment(row.OLD.START_TIME).format('ddd, MMMM DD • HH:mm') : '-'}
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '55px' }}
        >
          <Icon className={classes.icon}>smArrowForward</Icon>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
        >
          {row.NEW.START_TIME ? moment(row.NEW.START_TIME).format('ddd, MMMM DD • HH:mm') : '-'}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '145px' }}
        >
          <span className={classes.info}>Request Status</span>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '245px' }}
        >
          {row.OLD.S_STATUS ? (
            <div
              className={classNames({
                [classes.status]: true,
                [classes.statusSuccess]:
                  row.OLD.S_STATUS === 'ACTIVE' || row.OLD.S_STATUS === 'COMPLETE',
                [classes.statusCancel]: row.OLD.S_STATUS === 'INACTIVE',
                [classes.statusWaiting]: row.OLD.S_STATUS === 'WAITING_APPROVE',
              })}
            >
              {showStatusSession(row.OLD.S_STATUS)}
            </div>
          ) : (
            '-'
          )}
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '55px' }}
        >
          <Icon className={classes.icon}>smArrowForward</Icon>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
        >
          {row.NEW.S_STATUS ? (
            <div
              className={classNames({
                [classes.status]: true,
                [classes.statusSuccess]:
                  row.NEW.S_STATUS === 'ACTIVE' || row.NEW.S_STATUS === 'COMPLETE',
                [classes.statusCancel]: row.NEW.S_STATUS === 'INACTIVE',
                [classes.statusWaiting]: row.NEW.S_STATUS === 'WAITING_APPROVE',
              })}
            >
              {showStatusSession(row.NEW.S_STATUS)}
            </div>
          ) : (
            '-'
          )}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '145px' }}
        >
          <span className={classes.info}>Completed At</span>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '245px' }}
        >
          {row.OLD.COMPLETED_AT ? moment(row.OLD.COMPLETED_AT).format('ddd, MMMM DD • HH:mm') : '-'}
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '55px' }}
        >
          <Icon className={classes.icon}>smArrowForward</Icon>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
        >
          -
        </TableCell>
      </TableRow>
    </TableBody>
  )
}
export default DetailSessionIncomplete
