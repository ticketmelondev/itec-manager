import React from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import moment from 'moment'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'

import Icon from '@components/Icon'

import DetailStyles from './DetailStyles'

function mapTeacherName(teacherId, accounts) {
  const teacher = accounts.find((account) => account.ACCOUNT_ID === teacherId)
  return `${teacher?.FISRT_NAME} ${teacher?.LAST_NAME}`
}
function mapLevelName(levelId, levels) {
  const level = levels.find((level) => level.LEVEL_ID === levelId)
  return `${level?.LEVEL}`
}

function DetailSessionEdit({ row, courses }) {
  const classes = DetailStyles()
  return (
    <TableBody>
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '145px' }}
        >
          <span className={classes.info}>Start Time</span>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '245px' }}
        >
          {row.OLD.START_TIME ? moment(row.OLD.START_TIME).format('ddd, MMMM DD • HH:mm') : '-'}
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '55px' }}
        >
          <Icon className={classes.icon}>smArrowForward</Icon>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
        >
          {row.NEW.START_TIME ? moment(row.NEW.START_TIME).format('ddd, MMMM DD • HH:mm') : '-'}
        </TableCell>
      </TableRow>
      {row.OLD.LEVEL_ID !== row.NEW.LEVEL_ID && (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Level</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
          >
            {row.OLD.LEVEL_ID !== '-'
              ? mapLevelName(
                  row.OLD.LEVEL_ID,
                  courses[row.NEW.COURSE_ID] ? courses[row.NEW.COURSE_ID].LEVEL : [],
                )
              : '-'}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
          >
            {row.NEW.LEVEL_ID !== '-'
              ? mapLevelName(
                  row.NEW.LEVEL_ID,
                  courses[row.NEW.COURSE_ID] ? courses[row.NEW.COURSE_ID].LEVEL : [],
                )
              : '-'}
          </TableCell>
        </TableRow>
      )}
      {row.OLD.TEACHER_ID !== row.NEW.TEACHER_ID && (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Teacher Name</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
          >
            {row.OLD.TEACHER_ID !== '-'
              ? mapTeacherName(
                  row.OLD.TEACHER_ID,
                  courses[row.NEW.COURSE_ID] ? courses[row.NEW.COURSE_ID].ACCOUNTS : [],
                )
              : '-'}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
          >
            {row.NEW.TEACHER_ID !== '-'
              ? mapTeacherName(
                  row.NEW.TEACHER_ID,
                  courses[row.NEW.COURSE_ID] ? courses[row.NEW.COURSE_ID].ACCOUNTS : [],
                )
              : '-'}
          </TableCell>
        </TableRow>
      )}
      {row.NEW.LINKS.length > 0 || row.OLD.LINKS.length > 0 ? (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Link</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
          >
            {row.OLD.LINKS.length > 0 ? row.OLD.LINKS : '-'}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
          >
            {row.NEW.LINKS.length > 0
              ? row.NEW.LINKS.map((link) => {
                  return <div>{link}</div>
                })
              : '-'}
          </TableCell>
        </TableRow>
      ) : null}
      {row.OLD.NOTE !== row.NEW.NOTE && (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Note</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
          >
            {row.OLD.NOTE ? row.OLD.NOTE : '-'}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
          >
            {row.NEW.NOTE ? row.NEW.NOTE : '-'}
          </TableCell>
        </TableRow>
      )}
    </TableBody>
  )
}
const mapStateToProps = (state) => {
  return {
    courses: state.activity.courses,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(DetailSessionEdit)
