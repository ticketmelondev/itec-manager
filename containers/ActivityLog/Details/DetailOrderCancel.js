import React from 'react'
import classNames from 'classnames'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'

import Icon from '@components/Icon'

import { showOrderStatus } from '@util/orderFunc'

import DetailStyles from './DetailStyles'

function DetailOrderCancel({ row }) {
  const classes = DetailStyles()
  return (
    <TableBody>
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '145px' }}
        >
          <span className={classes.info}>Payment Status</span>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '245px' }}
        >
          {row.OLD.STATUS ? (
            <div
              className={classNames({
                [classes.status]: true,
                [classes.statusSuccess]: row.OLD.STATUS === 'PAID',
                [classes.statusWaiting]:
                  row.OLD.STATUS === 'WAITING' || row.OLD.STATUS === 'ORDER_CREATE',
                [classes.statusCancel]: row.OLD.STATUS === 'CANCEL' || row.OLD.STATUS === 'REJECT',
              })}
            >
              {showOrderStatus(row.OLD.STATUS)}
            </div>
          ) : (
            '-'
          )}
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '55px' }}
        >
          <Icon className={classes.icon}>smArrowForward</Icon>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
        >
          {row.NEW.STATUS ? (
            <div
              className={classNames({
                [classes.status]: true,
                [classes.statusSuccess]: row.NEW.STATUS === 'PAID',
                [classes.statusWaiting]:
                  row.NEW.STATUS === 'WAITING' || row.NEW.STATUS === 'ORDER_CREATE',
                [classes.statusCancel]: row.NEW.STATUS === 'CANCEL' || row.NEW.STATUS === 'REJECT',
              })}
            >
              {showOrderStatus(row.NEW.STATUS)}
            </div>
          ) : (
            '-'
          )}
        </TableCell>
      </TableRow>
    </TableBody>
  )
}
export default DetailOrderCancel
