import { makeStyles } from '@material-ui/core/styles'

const ActivityLogStyles = makeStyles((theme) => ({
  typographyH4: {
    flexGrow: 1,
    fontSize: '2rem',
    fontWeight: 'normal',
    lineHeight: 2.4375,
    color: 'rgba(0, 0, 0, 0.7)',
  },
  buttonExport: {
    minWidth: '180px',
    height: '48px',
    marginLeft: 12,
    [theme.breakpoints.between('sm', 'md')]: {
      width: '-webkit-fill-available',
      marginLeft: 12,
    },
    [theme.breakpoints.down('xs')]: {
      width: '100%',
      marginLeft: 0,
      marginTop: 8,
    },
  },
  icon: {
    marginLeft: 10,
    fontSize: '1.125rem',
  },
  rootDatatable: {
    marginTop: 16,
  },
}))

export default ActivityLogStyles
