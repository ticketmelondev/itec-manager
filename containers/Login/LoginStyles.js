import { makeStyles } from '@material-ui/core/styles'

const LoginStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    height: '100vh',
  },
  contentRoot: {
    width: 360,
  },
  imgRoot: {
    width: 258,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  subtitle: {
    fontWeight: 'bold',
    fontSize: '18px',
    lineHeight: '143.16%',
    /* identical to box height, or 26px */
    letterSpacing: '0.04em',
    textTransform: 'capitalize',
    color: '#ED1B24',
  },
  fancy: {
    lineHeight: '0.5',
    textAlign: 'center',
    '& span': {
      display: 'inline-block',
      position: 'relative',
      '&:before, &:after': {
        content: `""`,
        position: 'absolute',
        height: '5px',
        borderBottom: '3px solid red',
        // borderTop: '1px solid red',
        top: 0,
        width: 125,
      },
      '&:before': {
        right: '100%',
        marginRight: '15px',
      },
      '&:after': {
        left: '100%',
        marginLeft: '15px',
      },
    },
  },
  inputRoot: {
    marginBottom: 16,
  },
  buttonRoot: {
    marginBottom: 24,
  },
  formControlLabel: {
    margin: 0,
  },
  radioRoot: {
    color: '#CCCCCC',
    padding: 0,
    marginRight: 8,
    '&$checked': {
      color: theme.palette.primary.main,
    },
  },
  checked: { color: theme.palette.primary.main },
  labelCheck: {
    fontSize: '14px',
    lineHeight: '20px',
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.56)',
  },
  alertRoot: {
    marginBottom: '24px',
    width: '360px',
  },
  alert: {
    color: theme.palette.primary.main,
    backgroundColor: '#FDE4E5',
    boxShadow: 'none',
    borderRadius: '5px',
  },
  alertIcon: {
    alignItems: 'center',
  },
}))

export default LoginStyles
