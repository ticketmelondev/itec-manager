import React from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/registerActions'
import * as AlertActions from '@actions/alertActions'
import { useForm } from 'react-hook-form'
import * as yup from 'yup'
import classNames from 'classnames'

import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Collapse from '@material-ui/core/Collapse'

import Input from '@components/Input'
import Button from '@components/Button'
import Icon from '@components/Icon'
import { Alert } from '@components/Snackbar'

import { getStatic } from '@lib/static'

import LoginStyles from './LoginStyles'

const BackdropLoading = dynamic(() => import('@components/BackdropLoading'), {
  ssr: false,
  loading: () => null,
})

const schema = yup.object().shape({
  EMAIL: yup.string().email().required(),
  PASSWORD: yup.string().required(),
})

function Login({ isLoading, actions, isAlert, alertActions }) {
  const { register, handleSubmit, errors } = useForm({
    validationSchema: schema,
  })
  const onSubmit = (data) => {
    actions.auth(data)
  }

  const classes = LoginStyles()

  return (
    <div className={classes.root}>
      <div className={classes.contentRoot}>
        <div className={classes.imgRoot}>
          <img
            src={`${getStatic('static/images/Eduworld.png')}`}
            alt=""
            style={{ width: '100%' }}
          />
        </div>
        <p class={classNames(classes.subtitle, classes.fancy)}>
          <span>Manager</span>
        </p>
        <div className={classes.alertRoot}>
          <Collapse in={isAlert.open}>
            <Alert
              classes={{ filledError: classes.alert, icon: classes.alertIcon }}
              severity={'error'}
              onClose={alertActions.handleClose}
            >
              {isAlert.text}
            </Alert>
          </Collapse>
        </div>

        <form onSubmit={handleSubmit(onSubmit)}>
          <div className={classes.inputRoot}>
            <Input
              id="email-input"
              name="EMAIL"
              label="Email"
              className={classes.margin}
              inputRef={register}
              error={errors.EMAIL}
              errorText={errors.EMAIL && errors.EMAIL.message}
            />
          </div>
          <div className={classes.inputRoot}>
            <Input
              id="password-input"
              name="PASSWORD"
              label="Password"
              type="password"
              className={classes.margin}
              inputRef={register}
              error={errors.PASSWORD}
              errorText={errors.PASSWORD && errors.PASSWORD.message}
            />
          </div>
          <div className={classes.inputRoot}>
            <FormControlLabel
              className={classes.formControlLabel}
              classes={{
                label: classes.labelCheck,
              }}
              control={
                <Checkbox
                  name="REMEMBER"
                  inputRef={register}
                  className={classes.radioRoot}
                  classes={{
                    checked: classes.checked,
                  }}
                  icon={<Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>uncheck</Icon>}
                  checkedIcon={
                    <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>checked</Icon>
                  }
                />
              }
              label="Remember me"
            />
          </div>
          <div className={classes.buttonRoot}>
            <Button type="submit" color="primary" size="large">
              Log In
            </Button>
          </div>
        </form>
      </div>
      {isLoading && <BackdropLoading open={isLoading} />}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoading.accessAccount,
    isAlert: state.isAlert,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
    alertActions: bindActionCreators(AlertActions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(Login)
