import { postAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import sha512 from '@lib/sha512'
import moment from 'moment'
import { AUTH_COOKIE_MAX_AGE, AUTH_COOKIE_ONE_YEAR_AGE } from '@config/constants'
import { setAlert } from './alertActions'

export const setPage = (page) => {
  return {
    type: 'SET_PAGE',
    page,
  }
}
export const setRegisterDetails = (details) => {
  return {
    type: 'SET_REGISTER_DETAILS',
    details,
  }
}
export const setAccessLoading = (isLoading) => {
  return {
    type: 'SET_ACCESS_ACCOUNT_LOADING',
    isLoading,
  }
}
export const setAccount = (payload) => {
  return {
    type: 'SET_ACCOUNT',
    payload,
  }
}

export const auth = (data) => async (dispatch, getState) => {
  const url = getAPIRouter('AUTH')
  const header = {
    'Content-Type': 'application/json',
  }
  const body = {
    ...data,
    EMAIL: data.EMAIL.toLowerCase(),
    PASSWORD: sha512(data.PASSWORD),
  }
  dispatch(setAccessLoading(true))
  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    const data = res.message.data
    if (data.detail.TYPE === 'ADMIN') {
      const payload = {
        ...res.message.data,
        exp: data.REMEMBER
          ? Math.floor(Date.now() / 1000) + AUTH_COOKIE_ONE_YEAR_AGE
          : Math.floor(Date.now() / 1000) + AUTH_COOKIE_MAX_AGE,
      }
      dispatch(setAccount(payload))
      location.href = `/`
    } else {
      dispatch(
        setAlert({
          text:
            'You do not have permission to access this server, Please choose a different account.',
          type: 'error',
        }),
      )
    }
  } else {
    console.error('error', res)
    dispatch(
      setAlert({
        text: 'Email address or password is either incorrect or not registered to our system',
        type: 'error',
      }),
    )
  }
  dispatch(setAccessLoading(false))
}

export const rgisterAccount = () => async (dispatch, getState) => {
  const { registerDetails } = getState().register

  const url = getAPIRouter('ACCOUNT')
  const header = {
    'Content-Type': 'application/json',
  }
  const body = {
    ...registerDetails,
    PASSWORD: sha512(registerDetails.PASSWORD),
    DOB: moment(registerDetails.DOB, 'D MMM YYYY').valueOf(),
  }
  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    console.log('res', res)
  } else {
    console.error('error', res)
  }
}

export const ValidateEmail = async (value) => {
  const url = getAPIRouter('SERVICE_EMAIL')
  const header = {
    'Content-Type': 'application/json',
  }
  const body = {
    EMAIL: value,
  }

  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    return true
  } else {
    return false
  }
}
