import { getAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import { randomString } from '@util/centerFunc'

export const setAllStudentsLoading = (isLoading) => {
  return {
    type: 'SET_ALL_STUDENTS_LOADING',
    isLoading,
  }
}

export const setAllStudentOriginal = (allStudentOriginal) => {
  return {
    type: 'SET_ALL_STUDENT_ORIGINAL',
    allStudentOriginal,
  }
}

export const setAllStudents = (allStudents) => {
  return {
    type: 'SET_ALL_STUDENTS',
    allStudents,
  }
}

export const setStudentDetail = (studentDetail) => {
  return {
    type: 'SET_STUDENT_DETAIL',
    studentDetail,
  }
}

export const initStudent = () => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_STUDENTS')
  const finalUrl = `${url}&${randomString(5)}`
  dispatch(setAllStudentsLoading(true))
  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const items = res.message.data.Items
    dispatch(setAllStudentOriginal(items))
    dispatch(setAllStudents(items))
  } else {
    //   dispatch(setNotFoundCourse(true))
    console.error('error', res)
  }
  dispatch(setAllStudentsLoading(false))
}
export const exportStudents = () => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_STUDENTS')
  const finalUrl = `${url}&export=true`
  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    window.location = `${res.message.data}`
  } else {
    console.error('error', res)
  }
}
