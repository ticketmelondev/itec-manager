import { getAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'

export const setAllCourses = (allCourses) => {
  return {
    type: 'SET_ALL_COURSES',
    allCourses,
  }
}

export const setCourse = (course) => {
  return {
    type: 'SET_COURSE',
    course,
  }
}

export const setNotFoundCourse = (isNotFoundCourses) => {
  return {
    type: 'SET_NOT_FOUND_COURS',
    isNotFoundCourses,
  }
}

export const setCourseLoading = (isLoading) => {
  return {
    type: 'SET_COURSE_LOADING',
    isLoading,
  }
}

export const initCourses = () => async (dispatch, getState) => {
  const url = getAPIRouter('GET_ALL_COURSES')
  const res = await getAPIService(url, {})
  if (res.status) {
    const items = res.message.data
    dispatch(setAllCourses(items))
  } else {
    console.error('error', res)
  }
}

export const initCourseDetail = (id) => async (dispatch, getState) => {
  const url = getAPIRouter('GET_COURSE_DETAILS')
  const finalUrl = url.replace('param', id)
  dispatch(setCourseLoading(true))
  const res = await getAPIService(finalUrl, {})
  if (res.status) {
    const items = res.message.data
    dispatch(setCourse(items))
  } else {
    dispatch(setNotFoundCourse(true))
    console.error('error', res)
  }
  dispatch(setCourseLoading(false))
}
