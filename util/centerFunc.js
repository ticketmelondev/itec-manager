export const randomString = (length) => {
  const randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  let result = ''
  for (let i = 0; i < length; i++) {
    result += randomChars.charAt(Math.floor(Math.random() * randomChars.length))
  }
  return result
}

export const displayShortID = (str) => {
  return str.substring(0, 7).toUpperCase()
}
export const displayStudentShortID = (str) => {
  return str.substring(0, 9).toUpperCase()
}
