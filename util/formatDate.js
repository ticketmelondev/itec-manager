import moment from 'moment'

export const toTimeFormat = () => {
  const x = 5 //minutes interval
  let times = [] // time array
  let tt = 0 // start time

  //loop to increment the time and push results in array
  for (let i = 0; tt < 24 * 60; i++) {
    let hh = Math.floor(tt / 60) // getting hours of day in 0-24 format
    let mm = tt % 60 // getting minutes of the hour in 0-55 format
    // const time = ('0' + hh).slice(-2) + ':' + ('0' + mm).slice(-2)
    times[i] = ('0' + hh).slice(-2) + ':' + ('0' + mm).slice(-2)
    tt = tt + x
  }
  return times
}
export const toDateTimeFormat = (timeSteamp, timezone = 'Asia/Bangkok') => {
  const dateTime = moment(timeSteamp)
  const output = {
    date: dateTime.format('D MMM YYYY'),
    time: {
      hour: dateTime.format('HH'),
      min: dateTime.format('mm'),
    },
  }
  return output
}

export const getHours = () => {
  let hours = []
  for (var i = 0; i < 24; i++) {
    let number = preDigits(i, 2)
    hours.push({ label: number, value: number })
  }
  return hours
}
export const getMinutes = () => {
  let minutes = []
  for (var i = 0; i < 60; i += 5) {
    let number = preDigits(i, 2)
    minutes.push({ label: number, value: number })
  }
  return minutes
}
export const preDigits = (number, digits) => {
  return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number
}
export const displayTime = (time) => {
  const h = Math.floor(time / 3600)
  const m = Math.floor((time % 3600) / 60)
  const s = Math.floor((time % 3600) % 60)
  if (h > 0) return `${h} Hours`
  else if (m > 0) return `${m} Minutes`
  return
}

export const toTimeSteampSchedule = (type, start_date, start_time, end_date, end_time) => {
  let weeks = 0
  let outputStart = []
  let outputEnd = []

  if (type === 'daily') {
    const diffDate = moment(end_date).diff(moment(start_date), 'days')
    for (let i = 0; i <= diffDate; i++) {
      const nextDay = moment(start_date, 'D MMM YYYY').add(i, 'days')
      const startTimeSteamp = toTimeSteampDateTime(nextDay, start_time)
      const endTimeSteamp = toTimeSteampDateTime(nextDay, end_time)
      outputStart = [...outputStart, startTimeSteamp]
      outputEnd = [...outputEnd, endTimeSteamp]
    }
  } else if (type === 'weekly') {
    const diffWeek = moment(end_date).diff(moment(start_date), 'week')
    for (let i = 0; i <= diffWeek; i++) {
      const nextWeek = moment(start_date, 'D MMM YYYY').add(weeks, 'days')
      const startTimeSteamp = toTimeSteampDateTime(nextWeek, start_time)
      const endTimeSteamp = toTimeSteampDateTime(nextWeek, end_time)
      outputStart = [...outputStart, startTimeSteamp]
      outputEnd = [...outputEnd, endTimeSteamp]
      weeks = weeks + 7
    }
  } else {
    const diffDate = moment(end_date).diff(moment(start_date), 'days')
    if (diffDate > 0) {
      for (let i = 0; i <= diffDate; i++) {
        const nextDay = moment(start_date, 'D MMM YYYY').add(i, 'days')
        let startTimeSteamp = ''
        let endTimeSteamp = ''
        if (i === 0) {
          startTimeSteamp = toTimeSteampDateTime(nextDay, start_time)
        } else {
          startTimeSteamp = toTimeSteampDateTime(nextDay, '00:00')
        }
        if (diffDate === i) {
          endTimeSteamp = toTimeSteampDateTime(nextDay, end_time)
        } else {
          endTimeSteamp = toTimeSteampDateTime(nextDay, '23:59')
        }
        outputStart = [...outputStart, startTimeSteamp]
        outputEnd = [...outputEnd, endTimeSteamp]
      }
    } else {
      const startTimeSteamp = toTimeSteampDateTime(start_date, start_time)
      const endTimeSteamp = toTimeSteampDateTime(end_date, end_time)
      outputStart = [startTimeSteamp]
      outputEnd = [endTimeSteamp]
    }
  }
  return { SCHEDULE_START_TIME: outputStart, SCHEDULE_END_TIME: outputEnd }
}
export const toTimeSteampDateTime = (date, time) => {
  const dateTimeStr = `${moment(date).format('D MMM YYYY')} ${time}`
  const dateTimeFormat = moment(dateTimeStr, 'D MMM YYYY HH:mm')
  const timeSteamp = moment(dateTimeFormat).valueOf()
  return timeSteamp
}
