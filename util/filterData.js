import { showOrderStatus } from '@util/orderFunc'

export const filterDatatableOrders = (data, filter) => {
  const filterText = filter.toLowerCase()
  const filterFunc = (data) => {
    const orderId = data.ORDER_ID.toLowerCase().indexOf(filterText) > -1
    const studentId = data.STUDENT_ID.toLowerCase().indexOf(filterText) > -1
    const student = data.STUDENT.toLowerCase().indexOf(filterText) > -1
    const courseName = data.COURSE_NAME.toLowerCase().indexOf(filterText) > -1
    const phone = data.CONTACT_NO.toLowerCase().indexOf(filterText) > -1
    // const level = data.LEVEL ? data.LEVEL.toLowerCase().indexOf(filterText) > -1 : false
    const teacher = data.TEACHER ? data.TEACHER.toLowerCase().indexOf(filterText) > -1 : false
    const status = showOrderStatus(data.STATUS).toLowerCase().indexOf(filterText) > -1
    return orderId || studentId || student || courseName || phone || teacher || status
  }
  return data.filter(filterFunc)
}
export const filterDatatableStudents = (data, filter) => {
  const filterText = filter.toLowerCase()
  const filterFunc = (data) => {
    const studentId = data.ACCOUNT_ID.toLowerCase().indexOf(filterText) > -1
    const email = data.EMAIL.toLowerCase().indexOf(filterText) > -1
    const firstName = data.FISRT_NAME
      ? data.FISRT_NAME.toLowerCase().indexOf(filterText) > -1
      : false
    const lastName = data.LAST_NAME ? data.LAST_NAME.toLowerCase().indexOf(filterText) > -1 : false
    const nickName = data.NICK_NAME ? data.NICK_NAME.toLowerCase().indexOf(filterText) > -1 : false
    const phone = data.CONTACT_NO.toLowerCase().indexOf(filterText) > -1
    return studentId || email || firstName || lastName || nickName || phone
  }
  return data.filter(filterFunc)
}
export const filterDatatableActivityLog = (data, filter) => {
  const filterText = filter.toLowerCase()
  const filterFunc = (data) => {
    const orderId = data.ORDER_ID.toLowerCase().indexOf(filterText) > -1
    const activityType = data.ACTIVITY_TYPE.toLowerCase().indexOf(filterText) > -1
    const name = data.NAME.toLowerCase().indexOf(filterText) > -1
    const accountType = data.ACCOUNT_TYPE.toLowerCase().indexOf(filterText) > -1
    return orderId || activityType || name || accountType
  }
  return data.filter(filterFunc)
}

export const filterStudentById = (data, id) => {
  const filterFunc = (data) => {
    return data.ACCOUNT_ID === id
  }
  return data.filter(filterFunc)
}
export const filterCourseById = (data, id) => {
  const filterFunc = (data) => {
    return data.COURSE_ID === id
  }
  return data.filter(filterFunc)
}
export const filterTeacherDetailById = (data, id) => {
  const filterFunc = (data) => {
    return data.teacherId === id
  }
  return data.filter(filterFunc)
}
export const filterOrderDetailById = (data, id) => {
  const filterFunc = (data) => {
    return data.ORDER_ID === id
  }
  return data.filter(filterFunc)
}
export const filterSessionActivate = (data) => {
  const filterFunc = (data) => {
    return ['WAITING_APPROVE', 'ACTIVE', 'COMPLETE'].includes(data.S_STATUS)
  }
  return data.filter(filterFunc)
}
export const filterSessionReschedule = (data, type) => {
  const filterFunc = (data) => {
    return data.STATUS_NEW_START_TIME === type
  }
  return data.filter(filterFunc)
}
export const filterSessionStatusActivate = (data, type) => {
  const filterFunc = (data) => {
    return data.S_STATUS !== 'INACTIVE'
  }
  return data.filter(filterFunc)
}
export const filterSessionStatus = (data, type) => {
  const filterFunc = (data) => {
    return data.S_STATUS === type
  }
  return data.filter(filterFunc)
}
export const filterOrderSuccess = (data) => {
  const filterFunc = (data) => {
    return ['PAID', 'WAITING', 'ORDER_CREATE'].includes(data.DETAIL.STATUS)
  }
  return data.filter(filterFunc)
}
export const filterAccount = (data, id) => {
  const filterFunc = (data) => {
    return data.ACCOUNT_ID === id
  }
  return data.filter(filterFunc)
}
export const filterCourseId = (data, id) => {
  const filterFunc = (data) => {
    return data.DETAIL.COURSE_ID === id
  }
  return data.filter(filterFunc)
}
export const filterSessionCourseId = (data, id) => {
  const filterFunc = (data) => {
    return data.ORDER_ID === id
  }
  return data.filter(filterFunc)
}
